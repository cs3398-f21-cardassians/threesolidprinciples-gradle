package threesolid;

// Single responsibility was chosen for the manager file as it doesn't manage anything outside of its domain. 

class Manager {
	IWorkable worker;

	public void Manager() {

	}
	public void setWorker(IWorkable w) {
		worker=w;
	}
	public void manage() {
		worker.work();
	}
}
