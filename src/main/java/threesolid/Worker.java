package threesolid;

/*    Open Close: This class is written in such a way that allows for it to be expanded upon without requiring 
 *              changes to this class.
 */

class Worker implements IWorkable, IFeedable {
	  private String name = "";

  	public String getName() {
    	  return name;
  	}

  	public void setName(String name) {
      	this.name = name;
  	}

  	public String work() {  
  		if (name == "") {
       		return "I'm working already!";
    	}
    	else {
       		return name + " is working very hard!";
    	}
	  }

	  public String eat() {
		    if (name == "") {
       	    return "I'm eating already!";
        }
        else {
    		    return name + " is eating a double cheeseburger with special sauce and bacon flavored Skittles!";
        }
	  }

    public Boolean addpositive(int a, int b) {
        if ((a+b) > 0)
            return(true);
        return(false);
    }
}