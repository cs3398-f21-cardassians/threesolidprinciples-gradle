package threesolid;

/*  Interface: This class only implements the interfaces required for it to function, and nothing more
 *  Open Close: This class is written in such a way that allows for it to be expanded upon without requiring 
 *              changes to this class.
 */

class SuperWorker implements IWorkable, IFeedable{
	public String work() {
		return "I'm a super worker!";
	}

	public String eat() {
		return "I'm eating a super healthy meal";
	}
}