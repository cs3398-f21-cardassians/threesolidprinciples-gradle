package threesolid;

// Single responsibility was chosen for the Robot file because it dictates the responsibility, in this case "work" of the robot.

public class Robot implements IWorkable{
	public String work() {
		return "Fired up and ready to serve";
	}

}
