package threesolid;

/*  Interface: This class only implements the interfaces required for it to function, and nothing more
 *  Open Close: This class is written in such a way that allows for it to be expanded upon without requiring 
 *              changes to this class.
 *  Single Responsibility: Nothing in this class manages anything outside of it's domain. 
 */

interface IWorker {
	public String work();
	public String eat();
}   